import Vue from 'vue'
import Router from 'vue-router'

import Header from '@/components/Header'
import Footer from '@/components/Footer'
import Index from '@/components/Index'
import Login from '@/components/Login'
import SSID from '@/components/SSID'
import APN from '@/components/APN'
import DataStatistics from '@/components/DataStatistics'
import WANsetup from '@/components/WANsetup'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      components: {
        default: Index,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/Index',
      components: {
        default: Index,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/Login',
      components: {
        default: Login,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/ssid',
      components: {
        default: SSID,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/apn',
      components: {
        default: APN,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/DataStatistics',
      components: {
        default: DataStatistics,
        nav: Header,
        footer: Footer
      }
    },
    {
      path: '/WANsetup',
      components: {
        default: WANsetup,
        nav: Header,
        footer: Footer
      }
    },
  ]
})
