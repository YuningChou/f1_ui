// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import fontawesome from '@fortawesome/fontawesome';
import fontAwesomeIcon from '@fortawesome/vue-fontawesome';
import solid from '@fortawesome/fontawesome-free-solid';

import './assets/style.scss'

Vue.config.productionTip = false

fontawesome.library.add(solid)
Vue.component('font-awesome-icon', fontAwesomeIcon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
